import java.util.Iterator;

import edu.princeton.cs.algs4.StdOut;

public class Deque<Item> implements Iterable<Item> {
    private int size;
    private Node first = null;
    private Node last = null;
//    private Node oldLast = null;

    private class Node {
        Item value;
        Node next;
        Node previous;
    }

    private class DequeIterator implements Iterator<Item> {
        private Node current = first;

        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public Item next() {
//            if (current.next == null) {
//                throw new java.util.NoSuchElementException();
//            }
            Item item = current.value;
            current = current.next;
            return item;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

    }

    // construct an empty deque
    public Deque() {

        size = 0;
    }

    // is the deque empty?
    public boolean isEmpty() {
        return first == null;
    }

    // return the number of items on the deque
    public int size() {
        return size;
    }

    // add the item to the front
    public void addFirst(Item item) {
        if (item == null) {
            throw new IllegalArgumentException();
        }

        size++;
        Node oldFirst = first;

        first = new Node();
        first.value = item;
        first.next = oldFirst;
        if (this.size() != 1)
            oldFirst.previous = first;
        first.previous = null;
        if (this.size() == 1) {
            last = first;
        }

    }

    // add the item to the back
    public void addLast(Item item) {
        if (item == null) {
            throw new IllegalArgumentException();
        }
        size++;

        Node oldLast = last;

        last = new Node();
        last.value = item;
        last.next = null;
        last.previous = oldLast;
        oldLast.next = last;
        if (this.size() == 1)
            first = last;
//        else
//            oldLast = last;

    }

    // remove and return the item from the front
    public Item removeFirst() {
        if (isEmpty()) {
            throw new java.util.NoSuchElementException();
        }
        size--;
        Item value = first.value;
        first = first.next;
        if (this.size() == 0) {
            last = null;
            first = null;
        }
        return value;
    }

    // remove and return the item from the back
    public Item removeLast() {
        if (isEmpty()) {
            throw new java.util.NoSuchElementException();
        }
        size--;
        Item value = last.value;
        last = last.previous;
        if (this.size() == 0) {
            last = null;
            first = null;
        }
        return value;

    }

    // return an iterator over items in order from front to back
    public Iterator<Item> iterator() {

        return new DequeIterator();

    }

    // unit testing (required)
    public static void main(String[] args) {
        Deque<Integer> deck = new Deque<Integer>();
        System.out.println(deck.isEmpty());
        deck.addFirst(15);
        deck.addFirst(17);
        deck.addFirst(19);
        deck.addLast(40);
        deck.addLast(25);
        for (Object object : deck) {
            System.out.println(object);
        }

        System.out.println(deck.size());
        System.out.println(deck.removeFirst());
        System.out.println(deck.size());
        System.out.println(deck.removeFirst());
        System.out.println(deck.size());
//      System.out.println(deck.removeFirst());
//      System.out.println(deck.size());
//      System.out.println(deck.removeFirst());
//      System.out.println(deck.size());
//      System.out.println(deck.removeFirst());
//      System.out.println(deck.size());

        System.out.println(deck.removeLast());
        System.out.println(deck.size());
        System.out.println(deck.removeLast());
        System.out.println(deck.size());
        System.out.println(deck.removeLast());
        System.out.println(deck.size());
        System.out.println(deck.isEmpty());
        System.out.println();

    }

}